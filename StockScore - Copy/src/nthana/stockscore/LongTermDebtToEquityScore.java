package nthana.stockscore;

public class LongTermDebtToEquityScore {
	private LinearScore linear;
	
	public LongTermDebtToEquityScore(double lowerDE, double upperDE) {
		linear = new LinearScore(lowerDE, 1, upperDE, 0);
	}

	public double getScore(double x) {
		return linear.getScore(x);
	}

}
