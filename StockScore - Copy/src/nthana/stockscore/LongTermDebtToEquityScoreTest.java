package nthana.stockscore;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LongTermDebtToEquityScoreTest {
	private LongTermDebtToEquityScore calc;
	
	@BeforeEach
	void beforeEach() {
		calc = new LongTermDebtToEquityScore(0.30, 1.5);		
	}

	@Test
	void testCornerPoint1() {
		assertEquals(1.0, calc.getScore(0.3));
	}

	@Test
	void testCornerPoint2() {
		assertEquals(0.0, calc.getScore(2.0));
	}
	
	@Test
	void testInBetween() {
		assertEquals(0.41, calc.getScore(1.0), 0.01);
	}	

	@Test
	void testCutOff1() {
		assertEquals(1.0, calc.getScore(0.0));
	}

	@Test
	void testCutOff2() {
		assertEquals(0.0, calc.getScore(2.5));
	}
}
