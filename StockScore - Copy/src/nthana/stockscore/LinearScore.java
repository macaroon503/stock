package nthana.stockscore;

// Class Temp
public class LinearScore {
	// Temp Var double type
	private double x1;
	private double y1;
	private double x2;
	private double y2;
	
	// Function Temp
	public LinearScore(double x1, double y1, double x2, double y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		assert(x1 < x2);
	}

	public double getScore(double x) {
		return fullCutOff(linear(x));
	}

	// linear equation : 
	//      slope = (y-y1) / (x-x1)
	//      y = slope*(x-x1) + y1
	private double linear(double x) {
		return getSlope() * (x-x1) + y1;
	}
	
	private double getSlope() {
		return (y1-y2)/(x1-x2);
	}

	private double fullCutOff(double y) {
		double lowerCut = lowerCutOff(y);
		double fullCut = upperCutOff(lowerCut);
		return fullCut;
	}

	private double lowerCutOff(double y) {
		return Math.max(Math.min(y1, y2), y);
	}

	private double upperCutOff(double y) {
		return Math.min(Math.max(y1, y2), y);
	}

}
