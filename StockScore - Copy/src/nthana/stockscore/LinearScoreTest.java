package nthana.stockscore;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LinearScoreTest {

	@Test
	void test45DegreeLine() {
		LinearScore calculator = new LinearScore(0.0, 0.0, 1.0, 1.0);
		double score = calculator.getScore(0.5);
		
		assertEquals(0.5, score);
	}

	@Test
	void testSteepSlope() {
		LinearScore calculator = new LinearScore(0.0, 0.0, 1.0, 2.0);
		double score = calculator.getScore(0.5);
		
		assertEquals(1, score);
	}

	@Test
	void testUpperCutOff() {
		LinearScore calculator = new LinearScore(0.0, 0.0, 1.0, 2.0);
		double score = calculator.getScore(10);
		
		assertEquals(2, score);
	}

	@Test
	void testReverseSlope() {
		LinearScore calculator = new LinearScore(0.5, 1.0, 1.5, 0.0);
		assertEquals(0, calculator.getScore(1.5));
	}

	@Test
	void testReverseSlope2() {
		LinearScore calculator = new LinearScore(0.5, 1.0, 1.5, 0.0);
		assertEquals(0, calculator.getScore(2));
	}

	@Test
	void testReverseSlope3() {
		LinearScore calculator = new LinearScore(0.5, 1.0, 1.5, 0.0);
		assertEquals(1, calculator.getScore(0));
	}
}
